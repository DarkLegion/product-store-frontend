import { CartService } from './../../shared/services/cart.service';
import { ProductService } from './../../shared/services/product.service';
import { ConstantsService } from './../../core/services/constants.service';
import { ProductModel } from './../../shared/models/product.model';
import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProductListAbstract } from "app/shared/abstract/product-list.abstract";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent extends ProductListAbstract  {

  constructor(
    protected route: ActivatedRoute,
    protected CONSTANS: ConstantsService,
    protected productService: ProductService,
    private cartService: CartService
  ) {
    super(route,CONSTANS, productService);
  }


  addProduct(product: ProductModel) {
    this.cartService.addProduct(product);
  }

}
