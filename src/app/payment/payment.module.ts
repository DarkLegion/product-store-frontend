import { ProductService } from 'app/shared/services/product.service';
import { OrderProductService } from 'app/shared/services/order-product.service';
import { PaymentRoutingModule } from './payment-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    PaymentRoutingModule
  ],
  providers:[
    ProductService,
    OrderProductService
  ],
  declarations: []
})
export class PaymentModule { }
