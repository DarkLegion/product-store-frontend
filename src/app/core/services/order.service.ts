import { OrderModel } from './../../shared/models/order.model';
import { ConstantsService } from './../../core/services/constants.service';;
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { ApiServiceAbstract } from "app/shared/abstract/api-service.abstract";

@Injectable()
export class OrderService extends ApiServiceAbstract<OrderModel> {

  currentOrder:OrderModel;

  constructor(
    protected http: Http,
    protected CONSTANTS: ConstantsService
  ) {
    super(http, CONSTANTS, 'PAYMENT', 'orders');
  }
}
