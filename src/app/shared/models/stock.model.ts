export interface StockModel {
  _id: string,
  quantity: number,
  price: number,
  date: Date
}
