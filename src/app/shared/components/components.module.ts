import { OrderComponent } from './components/order/order.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyInfoComponent } from './company-info/company-info.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations:[
    OrderComponent,
    CompanyInfoComponent
  ],
  exports:[
    CompanyInfoComponent
  ]
})
export class ComponentsModule { }
