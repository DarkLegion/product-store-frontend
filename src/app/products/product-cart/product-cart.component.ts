import { OrderService } from './../../core/services/order.service';
import { ProductService } from './../../shared/services/product.service';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { ProductModel } from './../../shared/models/product.model';
import { CartService } from './../../shared/services/cart.service';
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-product-cart',
  templateUrl: './product-cart.component.html',
  styleUrls: ['./product-cart.component.scss']
})
export class ProductCartComponent extends UnsubscriberComponentAbstract implements OnInit {
  products: ProductModel[];

  constructor(
    private cartService: CartService,
    private productService: ProductService,
    private router: Router,
    private orderService: OrderService
  ) {
    super();
  }

  ngOnInit() {
    this.products = this.cartService.getAddedProducts();
  }
  calculteTotal(): number {
    return this.productService.calculateTotalPrice(this.products);
  }

  checkout() {
    this.orderService.create({
      orderProducts: this.cartService.genOrderProducts()
    })
      .takeUntil(this.ngUnsubscribe)
      .subscribe((order) => {
        this.router.navigate(['/payment', {id:order._id}])
      })

  }
}
