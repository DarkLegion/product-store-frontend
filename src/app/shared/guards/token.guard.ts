import { AuthService } from './../../core/services/auth.service';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import 'rxjs/add/operator/map';

@Injectable()
export class TokenGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    let userInfo = this.authService.getTokenInfo();
    if (userInfo) {
      let token = userInfo.token;
      return this.authService.validateToken(token).map(() => {
        return true;
      }).catch((e) => {
        this.router.navigate(['/admin']);
        return Observable.of(false);
      })
    }
    else {
      this.router.navigate(['/admin']);
      return false;
    }
  }
}
