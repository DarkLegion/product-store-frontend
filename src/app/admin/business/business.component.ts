import { ActivatedRoute } from '@angular/router';
import { CompanyModel } from './../../shared/models/company.model';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.scss']
})
export class BusinessComponent extends UnsubscriberComponentAbstract implements OnInit {

  company: CompanyModel;

  constructor(
    private route: ActivatedRoute,
  ) {
    super();
  }

  ngOnInit() {
    this.route.data.takeUntil(this.ngUnsubscribe).subscribe(params => {
      this.company = params['company'];
    });
  }

}
