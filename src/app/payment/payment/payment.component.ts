import { OrderService } from './../../core/services/order.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private orderService:OrderService
  ) {

  }

  ngOnInit() {
  }

}
