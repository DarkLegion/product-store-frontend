import { ConstantsService } from './../../core/services/constants.service';

import { Http, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

export abstract class ApiServiceAbstract<T> {
  protected apiConfig = this.CONSTANTS.ENDPOINTS[this.apiKey];
  protected endPoint = `${this.CONSTANTS.API}/${this.apiConfig.PATH}/${this.path}`;

  constructor(
    protected http: Http,
    protected CONSTANTS: ConstantsService,
    protected apiKey: string,
    protected path: string
  ) { }

  create(object: T | FormData, options?: RequestOptionsArgs): Observable<T> {
    return this.http.post(`${this.endPoint}`, object, options).map(res => res.json());
  }

  find(filter: any = {}, skip?: number, limit?: number, options?: RequestOptionsArgs) {
    return this.http.get(`${this.endPoint}?filter=${JSON.stringify(filter)}&skip=${skip}&limit=${limit}`, options).map(res => res.json());
  }

  destroy(id: string, options?: RequestOptionsArgs): Observable<T> {
    return this.http.delete(`${this.endPoint}/${id}`, options).map(res => res.json());
  }

  findById(id: string, options?: RequestOptionsArgs): Observable<T> {
    return this.http.get(`${this.endPoint}/${id}`, options).map(res => res.json());
  }

  findOne(filter: any = {}, options?: RequestOptionsArgs): Observable<T> {
    return this.http.get(`${this.endPoint}?findOne=true&filter=${JSON.stringify(filter)}`, options).map(res => res.json());
  }

  update(id, object: T | FormData, options?: RequestOptionsArgs): Observable<T> {
    return this.http.put(`${this.endPoint}/${id}`, object, options).map(res => res.json());
  }

}
