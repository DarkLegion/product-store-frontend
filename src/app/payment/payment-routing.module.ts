import { ComponentsModule } from './../shared/components/components.module';
import { OrderComponent } from './../shared/components/components/order/order.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreditCardComponent } from './credit-card/credit-card.component';
import { ConfigurationService } from './../shared/services/configuration.service';
import { CommonModule } from '@angular/common';
import { OrderResolver } from './../shared/resolvers/order.resolver';
import { PaymentComponent } from './payment/payment.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductService } from "app/shared/services/product.service";

const PaymentRoutes: Routes = [
  {
    path: '',
    component: PaymentComponent,
    resolve: {
      order: OrderResolver
    },
    children: [
      {
        path: '',
        component: OrderComponent,
        data: {
          mode: 'payment'
        }
      }, {
        path: 'creditCard',
        component: CreditCardComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(PaymentRoutes),
    CommonModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [
    PaymentComponent,
    CreditCardComponent
  ],
  providers: [
    OrderResolver,
    ConfigurationService
  ],
  exports: [
    PaymentComponent
  ]
})
export class PaymentRoutingModule { }
