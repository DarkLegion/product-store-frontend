import { AuthService } from './../../core/services/auth.service';
import { Observable } from 'rxjs/Rx';
import { ConstantsService } from './../../core/services/constants.service';
import { ProductModel } from './../models/product.model';
import { Http, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { ApiServiceAbstract } from "app/shared/abstract/api-service.abstract";

@Injectable()
export class ProductService extends ApiServiceAbstract<ProductModel> {
  constructor(
    protected http: Http,
    protected CONSTANTS: ConstantsService,
    private authService: AuthService
  ) {
    super(http, CONSTANTS, 'INVENTORY', 'products');
  }

  getImgFullUrl(imgName: string): string {
    return `${this.CONSTANTS.API}${this.CONSTANTS.ENDPOINTS.INVENTORY.ASSETS}/img/${imgName}`;
  }

  calculateTotalPrice(products: ProductModel[]): number {
    let total = 0;
    products.map((product) => {
      total += product.quantity * product.stocks[0].price;
    })
    return total;
  }

  private genOrderHeadersAndFormData(object: ProductModel, img: File): { formData: FormData, tokenHeader: RequestOptions } {
    let tokenHeader = this.authService.genTokenHeaders();
    let formData: FormData = new FormData();
    formData.append('img', img);
    formData.append('name', object.name);
    formData.append('stocks', JSON.stringify([object['stock']]));
    tokenHeader.headers.append('enctype', 'multipart/form-data');
    return {
      formData,
      tokenHeader
    }
  }

  create(object: ProductModel, img: File): Observable<ProductModel> {
    let genHeadersAndForm = this.genOrderHeadersAndFormData(object, img)
    return super.create(genHeadersAndForm.formData, genHeadersAndForm.tokenHeader);
  }

  update(id, object: ProductModel, img: File): Observable<ProductModel> {
    let genHeadersAndForm = this.genOrderHeadersAndFormData(object, img)
    return super.update(id, genHeadersAndForm.formData, genHeadersAndForm.tokenHeader);
  }

  destroy(id: string): Observable<ProductModel> {
    return super.destroy(id, this.authService.genTokenHeaders());
  }
}
