import { OrderService } from './../core/services/order.service';
import { CartService } from './../shared/services/cart.service';
import { ProductsRoutingModule } from './products-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule
  ],
  providers:[
    CartService,
    OrderService
  ]
})
export class ProductsModule { }
