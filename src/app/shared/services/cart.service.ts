import { OrderProductModel } from './../models/order-product.model';
import { ConstantsService } from './../../core/services/constants.service';
import { ProductModel } from './../models/product.model';
import { Injectable } from '@angular/core';

@Injectable()
export class CartService {

  private addedProducts: ProductModel[] = [];
  private total: number = 0;

  constructor(
    protected CONSTANTS: ConstantsService
  ) { }

  cleanAddedProducts() {
    this.total = 0;
    this.addedProducts = [];
  }

  getAddedProducts(): ProductModel[] {
    return this.addedProducts;
  }

  findProduct(_id: string): ProductModel {
    return this.addedProducts.find((product) => {
      return product._id === _id;
    })
  }

  createProduct(newProduct: ProductModel): ProductModel {
    let productExists = this.addedProducts.find((product) => {
      return product._id === newProduct._id;
    });
    if (productExists) {
      return productExists;
    } else {
      newProduct.quantity = 0;
      this.addedProducts.push(newProduct);
      return newProduct;
    }
  }

  addProduct(product: ProductModel): void {
    this.createProduct(product);
    this.total++;
    product.quantity++;
  }

  removeProduct(product: ProductModel) {
    let index = this.addedProducts.indexOf(product);
    if (index) {
      this.total -= product.quantity;
      this.addedProducts.splice(
        index,
        1
      )
    }
  }

  getTotal(): number {
    return this.total;
  }

  changeProductQuantity(product: ProductModel, quantity: number) {
    let cartProduct = this.createProduct(product);
    if (quantity < cartProduct.quantity) {
      this.total -= (cartProduct.quantity - quantity);
    } else if (quantity > cartProduct.quantity) {
      this.total += (quantity - cartProduct.quantity);
    }
    cartProduct.quantity = quantity;
  }

  genOrderProducts(): OrderProductModel[] {
    return this.addedProducts.map((product) => {
      return {
        product: product._id,
        quantity: product.quantity
      }
    });
  }
}
