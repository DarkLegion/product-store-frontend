import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: 'app/products/products.module#ProductsModule'
  },{
    path:'payment',
    loadChildren:'app/payment/payment.module#PaymentModule'
  },
  {
    path:'success',
    loadChildren:'app/success/success.module#SuccessModule'
  },{
    path:'admin',
    loadChildren:'app/admin/admin.module#AdminModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
