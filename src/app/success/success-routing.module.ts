import { OrderProductService } from 'app/shared/services/order-product.service';
import { ProductService } from 'app/shared/services/product.service';
import { ComponentsModule } from './../shared/components/components.module';
import { OrderComponent } from './../shared/components/components/order/order.component';
import { CompanyService } from './../shared/services/company.service';
import { CompanyResolver } from './../shared/resolvers/company.resolver.';
import { SuccessComponent } from './success/success.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const SuccessRoutes: Routes = [
  {
    path: '',
    component: SuccessComponent,
    resolve: {
      company: CompanyResolver
    },
    children: [
      {
        path: '',
        component: OrderComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(SuccessRoutes),
    ComponentsModule
  ],
  providers: [
    CompanyService,
    CompanyResolver,
    ProductService,
    OrderProductService
  ],
  declarations: [
    SuccessComponent
  ],
  exports: [
    RouterModule
  ]
})
export class SuccessRoutingModule { }
