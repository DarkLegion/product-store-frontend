import { ConfigurationService } from './../services/configuration.service';
import { ConfigurationModel } from './../models/configuration.model';
import { ConstantsService } from './../../core/services/constants.service';
import { Observable } from 'rxjs/Rx';
import {
  Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import 'rxjs/add/operator/first';
import { Injectable } from '@angular/core';

@Injectable()
export class ConfigurationListResolver implements Resolve<Observable<ConfigurationModel[]>> {

  constructor(
    private configurationService: ConfigurationService,
    private CONSTANTS: ConstantsService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<ConfigurationModel[]> {
    let filterParam = route.params.filter;
    let filter = filterParam ? JSON.parse(filterParam) : {};
    return this.configurationService.find(filter, 0, this.CONSTANTS.ENDPOINTS.INVENTORY.LIMIT).first();
  }
}
