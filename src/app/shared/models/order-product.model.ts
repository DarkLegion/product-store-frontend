export interface OrderProductModel {
  date?: Date,
  _id?: string,
  product: string,
  quantity: number
}
