export interface CreditCardModel {
  _id: string,
  name: String,
  number: String,
  expiration: String,
  securityCode: String
}