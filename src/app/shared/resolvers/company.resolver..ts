import { CompanyService } from './../services/company.service';
import { CompanyModel } from './../models/company.model';
import { ProductService } from './../services/product.service';
import { Observable } from 'rxjs/Rx';
import {
  Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import 'rxjs/add/operator/first';
import { Injectable } from '@angular/core';

@Injectable()
export class CompanyResolver implements Resolve<Observable<CompanyModel>> {

  constructor(
    private companyService: CompanyService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<CompanyModel> {
    return this.companyService.find().first();
  }
}
