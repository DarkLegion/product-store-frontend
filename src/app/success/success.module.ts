import { SuccessRoutingModule } from './success-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    SuccessRoutingModule
  ],
  declarations: []
})
export class SuccessModule { }
