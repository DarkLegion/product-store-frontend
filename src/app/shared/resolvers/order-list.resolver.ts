import { ConstantsService } from './../../core/services/constants.service';
import { OrderService } from './../../core/services/order.service';
import { ConfigurationService } from './../services/configuration.service';
import { ProductService } from './../services/product.service';
import { OrderModel } from './../models/order.model';
import { Observable } from 'rxjs/Rx';
import {
  Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { Injectable } from '@angular/core';

@Injectable()
export class OrderListResolver implements Resolve<Observable<OrderModel[]>> {

  constructor(
    private orderService: OrderService,
    private productService: ProductService,
    private CONSTANTS: ConstantsService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<OrderModel[]> {
    let filterParam = route.params.filter;
    let limit = route.params.limit || this.CONSTANTS.ENDPOINTS.PAYMENT.LIMIT;
    let filter = filterParam ? JSON.parse(filterParam) : {};
    return this.orderService.find(filter, 0, Number(limit)).first();
  }
}
