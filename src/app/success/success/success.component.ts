import { Router } from '@angular/router';
import { CompanyModel } from './../../shared/models/company.model';
import { ActivatedRoute } from '@angular/router';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent extends UnsubscriberComponentAbstract implements OnInit {

  companyInfo: CompanyModel;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {
    super();
  }

  ngOnInit() {
    this.route.data.takeUntil(this.ngUnsubscribe).subscribe(params => {
      this.companyInfo = params['company'];
    });
  }
  goToHomePage() {
    this.router.navigate(['/']);
  }
}
