import { ProductModel } from './product.model';
import { CreditCardModel } from './credit-card.model';
import { OrderProductModel } from './order-product.model';

export interface OrderModel {
  date?: Date,
  _id?: string,
  tax?: number,
  orderProducts: OrderProductModel[];
  creditCard?: CreditCardModel,
  products?: ProductModel[]
}
