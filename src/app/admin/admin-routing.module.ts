import { ConfigurationListResolver } from './../shared/resolvers/configuration-list.resolver';
import { ConfigurationComponent } from './configuration/configuration.component';
import { CompanyService } from './../shared/services/company.service';
import { CompanyResolver } from './../shared/resolvers/company.resolver.';
import { BusinessComponent } from './business/business.component';
import { OrderProductService } from './../shared/services/order-product.service';
import { ConfigurationService } from './../shared/services/configuration.service';
import { OrderResolver } from './../shared/resolvers/order.resolver';
import { OrderComponent } from './../shared/components/components/order/order.component';
import { ComponentsModule } from './../shared/components/components.module';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderListResolver } from './../shared/resolvers/order-list.resolver';
import { OrdersComponent } from './orders/orders.component';
import { ProductDetailResolver } from './../shared/resolvers/product-detail.resolver';
import { CreateEditProductComponent } from './create-edit-product/create-edit-product.component';
import { ProductComponent } from './product/product.component';
import { ProductsListComponent } from './product-list/products-list.component';
import { ProductService } from 'app/shared/services/product.service';
import { ProductListResolver } from './../shared/resolvers/product-list.resolver';
import { TokenGuard } from './../shared/guards/token.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const AdminRoutes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    canActivate: [TokenGuard],
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: ProductComponent,
        children: [
          {
            path: '',
            component: ProductsListComponent,
            resolve: {
              products: ProductListResolver
            },
          },
          {
            path: 'add',
            component: CreateEditProductComponent,
            data: {
              mode: 'create'
            }
          },
          {
            path: 'edit',
            component: CreateEditProductComponent,
            resolve: {
              product: ProductDetailResolver
            },
            data: {
              mode: 'edit'
            }
          }
        ]
      },
      {
        path: 'orders',
        component: OrdersComponent,
        children: [
          {
            path: '',
            component: OrderListComponent,
            resolve: {
              orders: OrderListResolver
            }
          }, {
            path: 'view',
            component: OrderComponent,
            resolve: {
              order: OrderResolver
            }
          }
        ]
      },
      {
        path: 'business',
        component: BusinessComponent,
        resolve: {
          company: CompanyResolver
        }
      },
      {
        path:'configuration',
        component:ConfigurationComponent,
        resolve:{
          configurations: ConfigurationListResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(AdminRoutes),
    CommonModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [
    LoginComponent,
    DashboardComponent,
    ProductsListComponent,
    ProductComponent,
    CreateEditProductComponent,
    OrdersComponent,
    BusinessComponent,
    OrderListComponent,
    ConfigurationComponent
  ],
  providers: [
    TokenGuard,
    ProductListResolver,
    ProductService,
    ProductDetailResolver,
    OrderListResolver,
    OrderResolver,
    ConfigurationService,
    OrderProductService,
    CompanyResolver,
    CompanyService,
    ConfigurationListResolver
  ],
  exports: [

  ]
})
export class AdminRoutingModule { }
