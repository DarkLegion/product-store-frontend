export interface ConfigurationModel {
  _id: string,
  name: String,
  value: String
}
