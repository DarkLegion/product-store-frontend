import { ProductModel } from './../../shared/models/product.model';
import { ProductService } from 'app/shared/services/product.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-edit-product',
  templateUrl: './create-edit-product.component.html',
  styleUrls: ['./create-edit-product.component.scss']
})
export class CreateEditProductComponent extends UnsubscriberComponentAbstract implements OnInit {
  private title: string
  mode: string;
  productForm: FormGroup;
  image: File;
  product: ProductModel;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private productService: ProductService
  ) {
    super();
  }

  ngOnInit() {
    this.route.data.takeUntil(this.ngUnsubscribe).subscribe(params => {
      this.mode = params['mode'];
      this.product = params['product'];
      this.initAccordingMode();
    });
  }

  private generateFormGroup(fill: boolean): void {
    this.productForm = this.fb.group({
      name: [fill ? this.product.name : ''],
      stock: this.fb.group({
        quantity: [fill ? this.product.stocks[0].quantity : 0],
        price: [fill ? this.product.stocks[0].price : 0]
      })
    });
  }

  fileChange($event) {
    this.image = $event.target.files[0];
  }

  save(){
    if(this.mode === 'edit'){
      this.update()
    }else{
      this.create()
    }
  }

  create() {
    this.productService.create(this.productForm.value, this.image).takeUntil(this.ngUnsubscribe).subscribe((model) => {
      this.router.navigate(['../'], { relativeTo: this.route })
    }, (e) => {
      console.error(e);
    })
  }

  update() {
    let object = this.productForm.value;
    object.stock._id = this.product.stocks[0]._id;
    this.productService.update(this.product._id, this.productForm.value, this.image).takeUntil(this.ngUnsubscribe).subscribe((model) => {
      this.router.navigate(['../'], { relativeTo: this.route })
    }, (e) => {
      console.error(e);
    })
  }

  private initAccordingMode(): void {
    if (this.mode === 'edit') {
      this.title = 'Edit Product',
      this.generateFormGroup(true);
    } else {
      this.title = 'Create Product'
      this.generateFormGroup(false);
    }
  }

}
