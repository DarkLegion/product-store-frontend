import { AuthService } from './../../core/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends UnsubscriberComponentAbstract implements OnInit {

  userForm: FormGroup;
  error: { message: string };

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private route:ActivatedRoute,
    private router: Router
  ) {
    super();
  }

  ngOnInit() {
    this.generateFormGroup();
  }

  private generateFormGroup(): void {
    this.userForm = this.fb.group({
      name: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    this.authService.login(this.userForm.value).takeUntil(this.ngUnsubscribe).subscribe((value) => {
      this.router.navigate(['./dashboard'], { relativeTo: this.route });
    }, (e) => {
      this.error = e;
    });
  }

}
