import { ProductModel } from './../models/product.model';
import { ProductService } from './../services/product.service';
import { Observable } from 'rxjs/Rx';
import {
  Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import 'rxjs/add/operator/first';
import { Injectable } from '@angular/core';

@Injectable()
export class ProductDetailResolver implements Resolve<Observable<ProductModel>> {

  constructor(
    private productService: ProductService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<ProductModel> {    
    return this.productService.findById(route.params.id).first();
  }
}