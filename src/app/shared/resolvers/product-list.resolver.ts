import { ProductModel } from './../models/product.model';
import { ConstantsService } from './../../core/services/constants.service';
import { ProductService } from './../services/product.service';
import { Observable } from 'rxjs/Rx';
import {
  Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import 'rxjs/add/operator/first';
import { Injectable } from '@angular/core';

@Injectable()
export class ProductListResolver implements Resolve<Observable<ProductModel[]>> {

  constructor(
    private productService: ProductService,
    private CONSTANTS: ConstantsService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<ProductModel[]> {
    let filterParam = route.params.filter;
    let filter = filterParam ? JSON.parse(filterParam) : {};
    return this.productService.find(filter, 0, this.CONSTANTS.ENDPOINTS.INVENTORY.LIMIT).first();
  }
}
