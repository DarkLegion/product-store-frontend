import { OrderService } from './../../core/services/order.service';
import { ConfigurationService } from './../services/configuration.service';
import { ProductService } from './../services/product.service';
import { OrderModel } from './../models/order.model';
import { Observable } from 'rxjs/Rx';
import {
  Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { Injectable } from '@angular/core';

@Injectable()
export class OrderResolver implements Resolve<Observable<OrderModel>> {

  constructor(
    private orderService: OrderService,
    private productService: ProductService,
    private configurationService: ConfigurationService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<OrderModel> {
    let order: OrderModel = null;
    return this.orderService.findById(route.params.id).flatMap((orderDB) => {
      order = orderDB;
      return this.configurationService.findById('tax').flatMap((config) => {
        order.tax = Number(config.value);
        return this.productService.find({
          _id: {
            $in:
            order.orderProducts.map((orderProduct) => {
              return orderProduct.product;
            })
          }
        }, 0, 0).map((products) => {
          products.forEach((product) => {
            let orderProduct = order.orderProducts.find((orderProduct) => {
              return orderProduct.product === product._id;
            });
            product.quantity = orderProduct.quantity;
          })
          order.products = products;
          this.orderService.currentOrder = order;
          return order;
        })
      })
    });
  }
}
