import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from './../../core/services/order.service';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { OrderModel } from './../../shared/models/order.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent extends UnsubscriberComponentAbstract implements OnInit {

  orderList: OrderModel[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private orderService: OrderService
  ) {
    super();
  }

  ngOnInit() {
    this.route.data.takeUntil(this.ngUnsubscribe).subscribe(params => {
      this.orderList = params['orders'];
    });
  }

  showOrder(order: OrderModel) {
    this.router.navigate(['../view', { id: order._id }],{relativeTo:this.route})
  }

  removeOrder(order: OrderModel) {
    this.orderService.destroy(order._id).takeUntil(this.ngUnsubscribe).subscribe((orderDB) => {
      let index = this.orderList.indexOf(order);
      this.orderList.splice(index, 1)
    })
  }

}
