import { CartService } from './../../shared/services/cart.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, AfterViewInit {

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {    
    this.cartService.cleanAddedProducts();
  }
  
  getQuantity() {
    return this.cartService.getTotal();
  }

}
