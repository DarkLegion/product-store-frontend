import { ProductStoreFrontendPage } from './app.po';

describe('product-store-frontend App', () => {
  let page: ProductStoreFrontendPage;

  beforeEach(() => {
    page = new ProductStoreFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
