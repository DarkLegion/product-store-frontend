import { OrderProductModel } from './../models/order-product.model';
import { ConstantsService } from './../../core/services/constants.service';;
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { ApiServiceAbstract } from "app/shared/abstract/api-service.abstract";

@Injectable()
export class OrderProductService extends ApiServiceAbstract<OrderProductModel> {
  constructor(
    protected http: Http,
    protected CONSTANTS: ConstantsService
  ) {
    super(http, CONSTANTS, 'PAYMENT', 'orderProducts');
  }
}
