import { UserModel } from './../../shared/models/user.model';
import { Observable } from 'rxjs/Rx';
import { ConstantsService } from './../../core/services/constants.service';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  private userInfo: { user: UserModel, token: string };

  private path = `${this.CONSTANTS.API}/${this.CONSTANTS.ENDPOINTS.BUSINESS.PATH}/auth`;
  constructor(
    protected http: Http,
    protected CONSTANTS: ConstantsService
  ) { }

  genTokenHeaders() {
    let headers = new Headers();
    headers.append("Authorization", `Bearer ${this.userInfo.token}`);
    headers.append('Accept', 'application/json');
    return new RequestOptions({headers});
  }

  login(user: UserModel): Observable<void> {
    return this.http.post(this.path, user).map((res) => {
      this.userInfo = res.json();
    });
  }

  validateToken(token): Observable<UserModel> {
    return this.http.post(`${this.path}/token`, { token }).map((res) => {
      return res.json();
    });
  }

  getTokenInfo(): { user: UserModel, token: string } {
    return this.userInfo;
  }

}
