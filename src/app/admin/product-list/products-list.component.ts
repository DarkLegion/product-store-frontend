import { Router } from '@angular/router';
import { ConstantsService } from './../../core/services/constants.service';
import { ProductListAbstract } from 'app/shared/abstract/product-list.abstract';
import { ProductService } from 'app/shared/services/product.service';
import { ActivatedRoute } from '@angular/router';
import { ProductModel } from './../../shared/models/product.model';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent extends ProductListAbstract {

  searchForm: FormGroup;
  error: string;

  constructor(
    protected route: ActivatedRoute,
    protected CONSTANS: ConstantsService,
    protected productService: ProductService,
    protected router: Router,
    private fb: FormBuilder
  ) {
    super(route, CONSTANS, productService);
  }

  ngOnInit() {
    this.generateFormGroup();
    super.ngOnInit();
  }

  search() {
    let value = this.searchForm.value;
    let limit = this.CONSTANS.ENDPOINTS.INVENTORY.LIMIT;
    if (value) {
      this.filter = {
        name: {
          $regex: value.name,
          $options: 'gi'
        }
      }
    } else {
      this.filter = {}
    }

    this.find(0, limit).takeUntil(this.ngUnsubscribe).subscribe((products) => {
      if (products.length === 0 || products.length < limit) {
        this.disableNext = true;
      } else {
        this.disableNext = false;
      }
      this.productList = products;
    });
  }

  private generateFormGroup(): void {
    this.searchForm = this.fb.group({
      name: ['']
    });
  }

  removeProduct(product: ProductModel): void {
    this.productService.destroy(product._id).takeUntil(this.ngUnsubscribe).subscribe((product) => {
      let limit = this.CONSTANS.ENDPOINTS.INVENTORY.LIMIT;
      this.find(0, limit).takeUntil(this.ngUnsubscribe).subscribe((products) => {
        this.search();
      });
    }, (e) => {
      console.error(JSON.parse(e._body).message);
    });
  }

  editProduct(product: ProductModel) {
    this.router.navigate(['../edit', { id: product._id }], { relativeTo: this.route })
  }

  createProduct() {
    this.router.navigate(['../add'], { relativeTo: this.route })
  }

}
