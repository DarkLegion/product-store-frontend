import { ConfigurationModel } from './../../shared/models/configuration.model';
import { ActivatedRoute } from '@angular/router';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent extends UnsubscriberComponentAbstract implements OnInit {
  
  configurations:ConfigurationModel[];

  constructor(
    private route:ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    this.route.data.takeUntil(this.ngUnsubscribe).subscribe(params => {
      this.configurations = params['configurations'];
    });
  }

}
