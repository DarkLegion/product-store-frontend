import { Injectable, Inject } from '@angular/core';

@Injectable()
export class ConstantsService {
  readonly API = 'http://localhost:3000';
  readonly ENDPOINTS = Object.freeze({
    INVENTORY: Object.freeze({
      PATH: 'inventory',
      get ASSETS() {
        return `/${this.PATH}/assets`
      },
      LIMIT: 10
    }),
    PAYMENT: Object.freeze({
      PATH: 'payment',
      LIMIT: 10
    }),
    BUSINESS: Object.freeze({
      PATH: 'business',
      LIMIT: 10
    })
  })
}
