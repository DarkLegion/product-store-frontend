import { ConstantsService } from './../../core/services/constants.service';
import { StockService } from './../services/stock.service';
import { StockModel } from './../models/stock.model';
import { Observable } from 'rxjs/Rx';
import {
  Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import 'rxjs/add/operator/first';
import { Injectable } from '@angular/core';

@Injectable()
export class StockResolver implements Resolve<Observable<StockModel>> {

  constructor(
    private stockService: StockService,
    private CONSTANTS: ConstantsService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<StockModel> {
    return this.stockService.findOne({ productId: route.params.id }).first();
  }
}