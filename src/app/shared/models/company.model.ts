export interface CompanyModel {
  _id: string,
  name: String,
  direction: String,
  contactNumber: String
}
