import { ProductDetailResolver } from './../shared/resolvers/product-detail.resolver';
import { ProductCartComponent } from './product-cart/product-cart.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { CommonModule } from '@angular/common';
import { ProductListResolver } from './../shared/resolvers/product-list.resolver';
import { ProductsComponent } from './products/products.component';
import { ProductListComponent } from './product-list/product-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductService } from "app/shared/services/product.service";

const productRoutes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    children: [
      {
        path: '',
        component: ProductListComponent,
        resolve: {
          products: ProductListResolver
        }
      },
      {
        path: 'cart',
        component: ProductCartComponent
      },
      {
        path: 'details/:id',
        component: ProductDetailComponent,
        resolve: {
          product: ProductDetailResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(productRoutes),
    CommonModule
  ],
  declarations: [
    ProductsComponent,
    ProductListComponent,
    ProductDetailComponent,
    ProductCartComponent
  ],
  providers: [
    ProductListResolver,
    ProductService,
    ProductDetailResolver
  ],
  exports: [
    ProductsComponent,
    ProductListComponent,
    ProductDetailComponent,
    ProductCartComponent
  ]
})
export class ProductsRoutingModule { }
