import { AuthService } from './services/auth.service';
import { OrderService } from './services/order.service';
import { ConstantsService } from './services/constants.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    ConstantsService,
    OrderService,
    AuthService
  ]
})
export class CoreModule { }
