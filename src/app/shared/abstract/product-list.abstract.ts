import { ProductModel } from './../models/product.model';
import { Observable } from 'rxjs/Rx';
import { ProductService } from './../../shared/services/product.service';
import { ConstantsService } from './../../core/services/constants.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { UnsubscriberComponentAbstract } from "app/shared/abstract/unsubscriber-component.abstract";


export abstract class ProductListAbstract extends UnsubscriberComponentAbstract implements OnInit {

  protected filter: any = {};

  skip: number = 0;
  disableNext = false;
  productList: ProductModel[];

  constructor(
    protected route: ActivatedRoute,
    protected CONSTANS: ConstantsService,
    protected productService: ProductService
  ) {
    super();
  }

  ngOnInit() {
    this.route.data.takeUntil(this.ngUnsubscribe).subscribe(params => {
      this.productList = params['products'];
    });
  }

  find(skip, limit): Observable<ProductModel[]> {
    return this.productService.find(this.filter, skip, limit).takeUntil(this.ngUnsubscribe)
  }

  next() {
    let defaultLimit = this.CONSTANS.ENDPOINTS.INVENTORY.LIMIT;
    let skip = this.skip + defaultLimit;
    this.find(skip, defaultLimit).subscribe((products) => {
      if (products.length) {
        this.skip = skip;
        this.productList = products;
      } else {
        this.disableNext = true;
      }
    });
  }

  previous() {
    let defaultLimit = this.CONSTANS.ENDPOINTS.INVENTORY.LIMIT;
    this.skip -= defaultLimit;
    this.productService.find(this.filter, this.skip, defaultLimit).takeUntil(this.ngUnsubscribe).subscribe((products) => {
      this.productList = products;
      this.disableNext = false;
    });
  }

  getImgFullUrl(imgName: string): string {
    return this.productService.getImgFullUrl(imgName);
  }

}
