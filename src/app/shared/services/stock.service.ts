import { StockModel } from './../models/stock.model';
import { ConstantsService } from './../../core/services/constants.service';;
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { ApiServiceAbstract } from "app/shared/abstract/api-service.abstract";

@Injectable()
export class StockService extends ApiServiceAbstract<StockModel> {
  constructor(
    protected http: Http,
    protected CONSTANTS: ConstantsService
  ) {
    super(http, CONSTANTS, 'INVENTORY', 'stocks');
  }
}