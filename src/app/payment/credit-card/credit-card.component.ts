import { OrderService } from './../../core/services/order.service';
import { Router } from '@angular/router';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { ActivatedRoute } from '@angular/router';
import { OrderModel } from './../../shared/models/order.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss']
})
export class CreditCardComponent extends UnsubscriberComponentAbstract implements OnInit {

  order: OrderModel;

  creditCardForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private orderService: OrderService,
     private router: Router,
  ) {
    super();
  }

  ngOnInit() {
    this.order = this.orderService.currentOrder;
    this.generateFormGroup();
  }

  updateOrderAndSaveCreditCard() {
    this.order.creditCard = this.creditCardForm.value;
    this.orderService.update(this.order._id, this.order).takeUntil(this.ngUnsubscribe).subscribe(()=>{
      this.router.navigate(['/success'])
    })
  }

  private generateFormGroup(): void {
    this.creditCardForm = this.fb.group({
      name: ['', Validators.required],
      number: ['', Validators.required],
      expirationDate: [''],
      securityCode: ['', Validators.required]
    });
  }
}
