import { OrderService } from './../../../../core/services/order.service';
import { OrderModel } from './../../../models/order.model';
import { OrderProductService } from './../../../services/order-product.service';
import { ProductService } from './../../../services/product.service';
import { ProductModel } from './../../../models/product.model';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent extends UnsubscriberComponentAbstract implements OnInit {

  order: OrderModel;
  mode: string;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private orderProductService: OrderProductService,
    private orderService: OrderService
  ) {
    super();
  }

  ngOnInit() {
    this.order = this.orderService.currentOrder;
    this.route.data.takeUntil(this.ngUnsubscribe).subscribe(params => {
      this.mode = params['mode'];
    });
  }

  getImageUrl(path): string {
    return this.productService.getImgFullUrl(path);
  }

  calculateTotalPrice(): number {
    let totalPrice = this.productService.calculateTotalPrice(this.order.products);
    return totalPrice + (totalPrice * (this.order.tax / 1000));
  }

  removeOrderProduct(product: ProductModel) {
    this.order.orderProducts.find((orderProduct) => {
      if (orderProduct.product === product._id) {
        this.orderProductService.destroy(orderProduct._id).takeUntil(this.ngUnsubscribe).subscribe(orderProduct => {
          let products = this.order.products;
          products.splice(products.indexOf(product), 1);
        });
        return true;
      }
    });
  }

}
