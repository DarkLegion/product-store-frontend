import { CartService } from './../../shared/services/cart.service';
import { StockModel } from './../../shared/models/stock.model';
import { ProductModel } from './../../shared/models/product.model';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'app/shared/services/product.service';
import { UnsubscriberComponentAbstract } from 'app/shared/abstract/unsubscriber-component.abstract';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent extends UnsubscriberComponentAbstract implements OnInit {
  @ViewChild('quantity') input: ElementRef;
  product: ProductModel;
  stock: StockModel;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService
  ) {
    super();
  }

  ngOnInit() {
    this.route.data.takeUntil(this.ngUnsubscribe).subscribe(params => {
      this.product = params['product'];
      let cartProduct = this.cartService.findProduct(this.product._id);
      this.product.quantity = cartProduct ? cartProduct.quantity : 0;
      this.stock = this.product.stocks[0];
    });
  }

  getImgFullUrl(imgName: string): string {
    return this.productService.getImgFullUrl(imgName);
  }

  changeProductQuantity(product: ProductModel) {
    let input = this.input.nativeElement;
    let quantity = Number(input.value);
    let value = 0;
    if (isNaN(quantity) || quantity <= 0) {
      input.value = value;
    } else {
      value = quantity;
    }
    this.cartService.changeProductQuantity(product, value);
  }
  calculateQuantity(): number {
    return this.stock.quantity - this.product.quantity
  }
}
