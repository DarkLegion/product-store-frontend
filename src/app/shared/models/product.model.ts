import { StockModel } from './stock.model';
export interface ProductModel {
  name: string,
  _id: string,
  stocks: StockModel[],
  quantity?: number;
  img: string
}